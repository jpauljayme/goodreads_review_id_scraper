from lxml import html
import json
import re
import requests

import certifi
import urllib3

from goodreads import client
import schedule
import time
import itertools
import threading
import simplejson
import xmltodict
from time import sleep

from bs4 import BeautifulSoup
from dateutil import parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

import pymysql
import pymysql.cursors as cursors
import logging

# Book Class to be filled with scraped book details and to be inserted to the db


class Book(object):

    title = ""
    author = ""
    id = 0
    isbn = ""
    rating = 0
    review_count = 300
    reviews = list()

###################################################################################


class Review(object):
    review_text = ""
    book_id = 0


class BookList(object):
    books = list()

################### USER-DEFINED FUNCTIONS NEEDED TO SCRAPE AND RETRIEVE BOOK INFO #####################


def get_book(book_id):
    # for index, elem in enumerate(book_ids):
    #     print("Getting book... id = "+ str(elem))
    #     book = gc.book(book_ids[index])
    #     bookItem = Book()
    #     bookItem.title = book.title
    #     bookItem.isbn = book.isbn
    #     bookItem.author = book.authors
    #     bookItem.rating = book.average_rating
    #     print(bookItem.title)
    #     print(bookItem.isbn)
    #     bookList.books.append(bookItem)
    #     time.sleep(3)
    print("Retrieving book with book id :: {} " .format(book_id))
    book = gc.book(book_id)
    bookItem = Book()
    bookItem.id = book_id
    bookItem.title = book.title
    bookItem.isbn = book.isbn
    authors = book.authors
    print(type(bookItem.id))
    if type(authors) is list:
        bookItem.author = ",".join(str(author) for author in authors)
    else:
        bookItem.author = book.authors

    bookItem.rating = book.average_rating

    print("\nTitle :: {}\nID :: {}\nISBN :: {}\n Author/s :: {}\nRating :: {}"
          .format(bookItem.title, bookItem.id, bookItem.isbn, bookItem.author, bookItem.rating))

    return bookItem


def get_review(review_ids, title , book_id):
    print("\nGetting reviews.....")
    count = 0
    #reviewList = list()

    conn = connect_db()
    cur = conn.cursor(cursors.DictCursor)
    # sql = "SELECT review_id FROM review_ids WHERE is_used = 0;"
    # cur.execute(sql)
    try:
        sql = "SELECT review_id FROM review_ids WHERE is_used = 0;"
        cur.execute(sql)
        print("\nTotal number of ids - ", cur.rowcount)
    except Exception as e:
        conn.rollback()
        print(repr(e))
    
    count = 0
    for id in cur:
        review_id = id['review_id']
        count +=1
        print("\nREVIEW ID # :: {}" .format(review_id))

        request_url_book = "https://www.goodreads.com/review/show.xml?key="+api_key+"&id="+review_id
        user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'
        headers = {'User-Agent': user_agent}
        
        while True:
            try:
                # Make a get request to get review count of book specified by isbn"
                #response = requests.get(request_url_book)
                print("\nGetting review with ID :: {} || review # :: {} ".format(review_id, count))
                response = requests.get(request_url_book, headers=headers, timeout=3)
            
                # Print the status code of the response.
                print("\nStatus code :: {} ".format(response.status_code))
                conn = connect_db()
                cur = conn.cursor()
                sql = "UPDATE review_ids SET is_used = 1 WHERE review_ids.review_id = %s;"
                
                try:
                    cur.execute(sql, review_id)
                    conn.commit()
                    print("\nSuccesfully updated is_used to 1 !")

                    #Insert book review
                    reviewData = xmltodict.parse(response.content)
                    review_text = reviewData["GoodreadsResponse"]["review"]["body"]
                    ret = insert_review(book_id, review_text, title)
                    if ret:
                        print("\nSuccesfully inserted review!")
                    else:
                        print("\nFailed to insert review!")
                        break

                except Exception as e:
                    print("\nFailed to update")
                    conn.rollback()
                    print(repr(e))

            except:
                print("Retrying :(((")
                time.sleep(20)
                continue
            break    


        # reviewList.append(review_text)
        time.sleep(0.75)
    
    print("Iterations - {}".format(count))
    conn.close()
    # return reviewList


    # for id in review_ids:
    #     count += 1
    #     if(count == 200):
    #         time.sleep(15)

    #     request_url_book = "https://www.goodreads.com/review/show.xml?key="+api_key+"&id="+id

    #     user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'

    #     headers = {'User-Agent': user_agent}
        
        
    #     while True:
    #         try:
    #             # Make a get request to get review count of book specified by isbn"
    #             #response = requests.get(request_url_book)
    #             print("\nGetting review with ID :: {} || review # :: {} ".format(id, count))
    #             response = requests.get(request_url_book, headers=headers, timeout=3)
            
    #             # Print the status code of the response.
    #             print("\nStatus code :: {} ".format(response.status_code))
        
    #         except:
    #             print("Retrying :(((")
    #             continue
    #         break    

    #     reviewData = xmltodict.parse(response.content)
    #     review = Review()

    #     review.review_text = reviewData["GoodreadsResponse"]["review"]["body"]


    #     reviewList.append(review)

    #     time.sleep(1)


    #return reviewList

def get_reviews(review_ids, title, book_id):
    print("\nGetting reviews.....")
    count = 0
    #reviewList = list()

    conn = connect_db()
    cur = conn.cursor(cursors.DictCursor)
    # sql = "SELECT review_id FROM review_ids WHERE is_used = 0;"
    # cur.execute(sql)
    try:
        sql = "SELECT review_id FROM review_ids WHERE is_used = 0;"
        cur.execute(sql)
        print("\nTotal number of ids - ", cur.rowcount)
    except Exception as e:
        conn.rollback()
        print(repr(e))
    
    count = 0
    reviewList = list()
    for id in cur:
        review_id = id['review_id']
        count +=1
        print("\nREVIEW ID # :: {}" .format(review_id))

        request_url_book = "https://www.goodreads.com/review/show.xml?key="+api_key+"&id="+review_id
        user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'
        headers = {'User-Agent': user_agent}
        
        while True:
            try:
                # Make a get request to get review count of book specified by isbn"
                #response = requests.get(request_url_book)
                print("\nGetting review with ID :: {} || review # :: {} ".format(review_id, count))
                response = requests.get(request_url_book, headers=headers, timeout=3)
            
                # Print the status code of the response.
                print("\n****Status code :: {} SUCCESSFULLY RETRIEVED REVIEW!!**** ".format(response.status_code))

                reviewData = xmltodict.parse(response.content)
                review_text = reviewData["GoodreadsResponse"]["review"]["body"]


                conn = connect_db()
                cur = conn.cursor()
                sql = "UPDATE review_ids SET is_used = 1 WHERE review_ids.review_id = %s;"
                
                try:
                    cur.execute(sql, review_id)
                    conn.commit()
                    print("\nSuccesfully updated is_used to 1 !")

                    #Insert book review

                    # ret = insert_review(book_id, review_text, title)
                    # if ret:
                    #     print("\nSuccesfully inserted review!")
                    # else:
                    #     print("\nFailed to insert review!")
                    #     break

                except Exception as e:
                    print("\nFailed to update")
                    conn.rollback()
                    print(repr(e))

            except:
                print("\n*******RETRYING... :(((")
                time.sleep(20)
                continue
            break    

        if not review_text:
            print("\n******EMPTY REVIEW.......")
        else:
            reviewList.append(review_text)
        
        time.sleep(0.75)
    
    print("Iterations - {}".format(count))
    conn.close()
    return reviewList

def get_review_ids(book_id):
    print("\nGetting review ids....\n")
    new_review_ids = []

    # 0 - 1. Change the range number depending on how many pages of reviews you want to scrape.
    for page in range(1, 11):
        sleep(4)

        print("\n\nAt page {}\n".format(page))

        # Change this url according to the book
        goodreads_url = "https://www.goodreads.com/book/show/{}?hide_last_page=true&amp;page={}".format(
            book_id, page)

        user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'

        headers = {'User-Agent': user_agent}
        page = requests.get(goodreads_url, headers=headers, verify=False, timeout = 15)
        page_response = page.text

        parser = html.fromstring(page_response)
        XPATH_REVIEW_ID = '//*[contains(concat(" ", @class, " "), " review ")]/@id'
        #response.xpath('//*[contains(concat(" ", @class, " "), " review ")]/@id').extract_first()
        review_ids = parser.xpath(XPATH_REVIEW_ID)

        print("\n")
        print(review_ids)
        print("\n")
        # Extract id and append to new review id list
        for id in review_ids:
            new_review_ids.append(id.split('_')[1])

    return new_review_ids


def insert_book(book):

    conn = connect_db()
    # Create a Cursor object to execute queries.
    cur = conn.cursor()

    insert_book_info = ("INSERT INTO books (title, author, rating, review_count, source, goodreads_id) "
                        "VALUES (%s, %s, %s, %s, %s, %s)")

    # for book in bookList.books:
    data_book = (book.title, book.author, book.rating,
                 book.review_count, "Goodreads", book.id)
    bookID = -1

    # Insert to table book
    try:
        print(insert_book_info)
        print(data_book)
        cur.execute(insert_book_info, data_book)

        # get book ID
        if cur.lastrowid:
            bookID = cur.lastrowid
            print("\nSuccessfully inserted book!\n")
            print('BOOK ID {} '.format(bookID))
            conn.commit()
            # Return book ID
            return bookID

        else:
            print('\n\nLast insert id not found')

        conn.commit()

    except Exception as e:
        conn.rollback()
        print(repr(e))

    return bookID


def insert_review(book_id, review_text, title):

    conn = connect_db()
    # Create a Cursor object to execute queries.
    cur = conn.cursor()
    
    file = open('Goodreads_Reviews.txt', 'a', encoding='utf-8')
    file.write("\nBOOK TITLE : {}\n" .format(title))

    # for review in reviews:
    #     insert_review = ("insert into reviews (book_id, review_text) "
    #                      "VALUES (%s, %s )")

    #     data_review = (book_id, review_text)
    #     print(data_review)

    #     try:
    #         cur.execute(insert_review, data_review)
    #         conn.commit()
    #         print("\nSuccesfully inserted!")
    #         # write list into file
    #         file.write("\nReview Text : {}\n" .format(review_text))
            
    #     except Exception as e:
    #         conn.rollback()
    #         print(repr(e))
    #         print(review.review_text)

    #file.write("\nTotal number of reviews :: {} ".format(len(reviews)))
    #file.close()

    insert_review = ("insert into reviews (book_id, review_text) "
                         "VALUES (%s, %s )")

    data_review = (book_id, review_text)
    print(insert_review)
    print(data_review)
    ret = False
    try:
        cur.execute(insert_review, data_review)
        conn.commit()
        #print("\nSuccesfully inserted!")
        # write list into file
        file.write("\nReview Text : {}\n" .format(review_text))
        file.close()
        ret = True
        # print("\nFinished inserting review!\n")
    except Exception as e:
        conn.rollback()
        print(repr(e))
        print(review_text)


    return ret


def insert_review_ids(review_ids):
    
    conn = connect_db()
    cur = conn.cursor()

    print("\nInserting review ids.... with length {} ".format(review_ids))
    for id in review_ids:
        insert_review_id = ("insert into review_ids (review_id) "
                         "VALUES (%s)")

        data_id = (id)
        print(insert_review_id)
        print(data_id)

        try:
            cur.execute(insert_review_id, data_id)
            conn.commit()
            print("Succesfully inserted id!")
        except Exception as e:
            conn.rollback()
            print(repr(e))
            print(data_id)

def insert_reviews(book_id, reviews, title):
    conn = connect_db()
    # Create a Cursor object to execute queries.
    cur = conn.cursor()
    
    file = open('Goodreads_Reviews.txt', 'a', encoding='utf-8')
    file.write("\nBOOK TITLE : {}\n" .format(title))

    for review_text in reviews:
        insert_review = ("insert into reviews (book_id, review_text) "
                         "VALUES (%s, %s )")

        data_review = (book_id, review_text)

        try:
            cur.execute(insert_review, data_review)
            conn.commit()
            print("\nSuccesfully inserted review!")
            # write list into file
            file.write("\nReview Text : {}\n" .format(review_text))
            
        except Exception as e:
            conn.rollback()
            print(repr(e))
            print(review_text)

    file.write("\nTotal number of reviews :: {} ".format(len(reviews)))
    file.close()



def connect_db():

    user = 'root'
    password = 'root'
    host = '127.0.0.1'
    database = 'book_reviews'

    print("\nConnecting to database......")

    conn = pymysql.connect(host=host, user=user, password=password,
                           db=database, )

    return conn

def get_last_book_id():
    conn = connect_db()
    cur = conn.cursor(cursors.DictCursor)

    sql = "SELECT goodreads_id FROM books ORDER BY book_id DESC LIMIT 1;"
    book_id = -1
    try:
        cur.execute(sql)
        conn.commit()
        book_id = cur.fetchone()['goodreads_id']
        print("Succesfully retrieved book id {} ".format(book_id))
    except Exception as e:
        conn.rollback()
        print(repr(e))
    finally:
        conn.close()

    return book_id
logging.basicConfig(
    format='[%(asctime)-15s] [%(name)s] %(levelname)s]: %(message)s',
    level=logging.DEBUG
)


# Book ID's
book_ids = [
    # 228696, #owl_moon DONE
    # 19543, #Where the Wild Things Are DONE
    # Caps for Sale: A Tale of a Peddler, Some Monkeys and Their Monkey Business (Caps for Sale #1) DONE
    # 775597,
    # 56728,  # You Are Special (Wemmicksville, #1)DONE
    # 460548,  # Go, Dog. Go!
    # 420297,  # Curious George (Curious George Original Adventures)
    # 197084,  # Are You My Mother?
    # 23772,  # Green Eggs and Ham
    # 7784,  # The Lorax
    # 475339,  # Madeline DONE
    # 105551,  # Fox in Socks DONE
    # 153542,  # Mike Mulligan and His Steam Shovel done
    # 268917,  # The Doll People (Doll People #1)DONE
    # 7779,  # Horton Hears a Who! (Horton the Elephant) #DONE
    #The Tale of Peter Rabbit (The World of Beatrix Potter: Peter Rabbit)
    # 19321, #DONE
    # 7846067,  # We are in a Book! (Elephant & Piggie, #13)
    # 774001,  # Amelia Bedelia  (Amelia Bedelia #1)
    # 24178,  # Charlotte's Web
    # 233093,  # The Cat in the Hat
    #30119,  # Where the Sidewalk Ends
    #370493,  # The Giving Tree
    #767680,  # If You Give a Mouse a Cookie (If You Give...)
    # The Lion, the Witch and the Wardrobe (The Chronicles of Narnia (Publication Order) #1)
    # 100915,
    # 11387515,  # Wonder
    # 113946,  # How the Grinch Stole Christmas!
    # 39988,  # Matilda
    # 8127,  # Anne of Green Gables (Anne of Green Gables #1)
    # 5907,  # The Hobbit (Middle-Earth Universe)
    # 144974,  # The Velveteen Rabbit
    # 6310,  # Charlie and the Chocolate Factory
    # 6319,  # The BFG
    # 157993  # The Little Prince
    # 7781,    #The 500 Hats of Bartholomew Cubbins
    # 551876, #Tikki_Tikki_Tembo
    # 515885, #But_Not_the_Hippopotamus
    # 65217, #The_Five_Chinese_Brothers
    # 23208632, #-gertie-s-leap-to-greatness
    # 825679, #The_World_According_to_Humphrey
    # 310259, #Love_You_Forever DONEE
    #348489, #Snowflake_Bentley
    #20518799, #the-iridescence-of-birds
    # 18851166, #flashlight
    # 20708766, #-leroy-ninker-saddles-up
    # 18254, #Oliver_Twist
    # 5326, #A_Christmas_Carol
    # 65605, #The_Magician_s_Nephew
    # 17061, #Coraline
    # 2998, #The_Secret_Garden
    # 3008, #A_Little_Princess
    # 10210, #Jane_Eyre
    # 191139, #Oh the Places You'll Go
    # 7770, #One Fish Two Fish Red Fish Blue Fish (I Can Read It All by Myself)
    # 41757, #Oh, the Thinks You Can Think! 
    # 7777,# McElligot's Pool (Classic Seuss)
    # 12127810, # The House of Hades (Heroes of Olympus, The, Book Four) (The Heroes of Olympus)
    # 12127750, # The Mark of Athena (Heroes of Olympus, Bk 3) (The Heroes of Olympus)
    # 17733898, # Diary of a Wimpy Kid: Hard Luck, Book 8
    # 21535019, # Diary of a Wimpy Kid: The Long Haul
    # 2623,# Great Expectations
    # 15881, # Harry Potter and the Chamber of Secrets
    # 136251, # Harry Poter and the Deathly Hollows
    # 1, #Harry_Potter_and_the_Half_Blood_Prince
    # 3, # Harry Potter and the Sorceror's Stone
    # 6, # Harry Potter and the Goblet of Fire
    # 5, # Harry Potter and the Prisoner of Azkaban
    # 2 #Harry Potter and the Order of the Phoenix
    # 34898, #My_Father_s_Dragon
    # 18743522, #-shh-we-have-a-plan
    # 14883914, #-my-grandfather-s-coat
    # 344458, #The_Hundred_Mile_An_Hour_Dog
    # 235846, #A_Child_s_Calendar
    # 1658143, #The_Apple_Pie_Tree
    # 12127455, #looking-at-lincoln
    # 20518801, #kid-sheriff-and-the-terrible-toads
    # 3168029, #the-moon-over-star
    # 12065943, #extra-yarn
    # 12576704, #z-is-for-moose
    # 15812157, #my-cold-plum-lemon-pie-bluesy-mood
    # 11737306, #one-cool-friend
    # 16101018, #the-day-the-crayons-quit
    # 12448586, #boy-bot
    # 16152082, #my-first-day
    # 13259987, #creepy-carrots
    # 6534132, #the-lion-and-the-mouse
    # 15815400, #exclamation-mark
    # 13083239, #the-fantastic-flying-books-of-mr-morris-lessmore
    # 11737022, #the-art-of-miss-chew
    # 457762, #Sylvester_and_the_Magic_Pebble
    # 1161994 #Martina_the_Beautiful_Cockroach
    # 4948, #The_Very_Hungry_Caterpillar
    # 759611, #Brown_Bear_Brown_Bear_What_Do_You_See_
    # 293595, #Chicka_Chicka_Boom_Boom
    # 44186, #The_Monster_at_the_End_of_this_Book
    # 231850, #Corduroy
    # 32929, #Goodnight_Moon
    46677, #Alexander_and_the_Terrible_Horrible_No_Good_Very_Bad_Day
    98573, #Harold_and_the_Purple_Crayon
    8073, #Cloudy With a Chance of Meatballs
    99111, #The_World_of_Winnie_the_Pooh
    24213, #Alice_s_Adventures_in_Wonderland_Through_the_Looking_Glass
    301736, #Guess_How_Much_I_Love_You
    125507, #The_True_Story_of_the_3_Little_Pigs
    19302, #Pippi_Longstocking
    824204, #The_Little_Engine_That_Could
    407429, #The_Stinky_Cheese_Man_and_Other_Fairly_Stupid_Tales
    786256, #Stellaluna
    505304, #The_Poky_Little_Puppy
    29291, #Make_Way_for_Ducklings
    18131, #A_Wrinkle_in_Time
    236093, #The_Wonderful_Wizard_of_Oz
    6689, #James_and_the_Giant_Peach
    191113, #Don_t_Let_the_Pigeon_Drive_the_Bus_
    378, #  The_Phantom_Tollbooth
    420282, #The_Polar_Express
    8337, #Little_House_in_the_Big_Woods
    3636, #The_Giver
    196970, #The_Night_Before_Christmas
    133526, #Blueberries_for_Sal
    201146, #Little_Bear
    773951, #The_Story_of_Ferdinand
    766020, #The_Rainbow_Fish
    # 2839, #Bridge_to_Terabithia
    # 6327, #The_Witches
    # 240130, #The_Paper_Bag_Princess
    # 147732, #Miss_Nelson_Is_Missing_
    # 858719, #Clifford_the_Big_Red_Dog
    # 858513, #Chrysanthemum
    # 310258, #The_Snowy_Day
    # 474858, #A_Bad_Case_of_Stripes
    # 30120, #Falling_Up
    # 766955, #Click_Clack_Moo
    # 22017663, #-my-first-travel-book
    # 105549, #The_Sneetches_and_Other_Stories
    # 3980, #From_the_Mixed_Up_Files_of_Mrs_Basil_E_Frankweiler
    # 125404, #The_Indian_in_the_Cupboard
    # 37190, #The_Tale_of_Despereaux
    # 356321, #The_Story_of_Babar
    # 15779, #Sideways_Stories_from_Wayside_School
    # 22013040, #-the-seven-natural-wonders-of-the-earth
    # 770051, #Olivia
    # 114345, #The_Little_House_Collection
    # 232109, #The_Mouse_and_the_Motorcycle
    # 24583, #The_Adventures_of_Tom_Sawyer
    # 11301, #Horton_Hatches_the_Egg
    # 27712, #The_Neverending_Story
    # 153540, #The_Little_House
    # 78039, #Ramona_the_Pest
    # 857448, #Harry_the_Dirty_Dog
    # 206962, #Hop_On_Pop
    # 281235, #Bunnicula
    # 30118, #A_Light_in_the_Attic
    # 420404 #Yertle_the_Turtle_and_Other_Stories
]


review_ids = list()
bookList = BookList()
bookItem = Book()
book_id = -1
reviews = list()
urllib3.disable_warnings()


# create a client instance to query Goodreads
api_key = "5xfXFYst4ekRDaQBBCncsw"
api_secret = "kAIdYKWCOFHWHFHZqDr50S0YmpaTz4WHBUZrzs4Ez9o"

# Initialize Goodreads wrapper
gc = client.GoodreadsClient(api_key, api_secret)

for b_id in book_ids:
    # Retrieve book and store on bookItem variable
    # Change book_ids index according to the book to be collected
    bookItem = get_book(b_id)
    # Insert book to database
    book_id = insert_book(bookItem)
    #book_id = get_last_book_id()

    if book_id != -1:
        #bookItem = get_book(book_id)
        print("\nBook id :: {} ".format(book_id))
        # Scrape review ids from goodreads and store on review_ids variable
        review_ids = get_review_ids(bookItem.id)
        #Insert review ids
        insert_review_ids(review_ids)
        # Get book reviews given review ids list and bookItem , store to reviews variable
        # get_review(review_ids, bookItem.title, book_id)
        reviews = get_reviews(review_ids, bookItem.title, book_id)
        #insert reviews to database
        insert_reviews(book_id, reviews, bookItem.title)

    else:
        print("\n********Unable to insert reviews.... No reviews obtained.**********")


#IF API KEY IS REFRESHED TO CONITNUE THE LAST BOOK 
# book_id = get_last_book_id()

# if book_id != -1:
#     bookItem = get_book(book_id)
#     #reviews = get_review(review_ids, bookItem.title, book_id)
#     get_review(review_ids, bookItem.title, book_id)
#     #insert_reviews(book_id, reviews, bookItem.title)
# else:
#     print("\n********Unable to insert reviews.... No reviews obtained.**********")