from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
import numpy as np
import pymysql
import logging
from time import time
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from sklearn.metrics import silhouette_score, silhouette_samples
from sklearn.manifold import TSNE
from sklearn.decomposition import TruncatedSVD
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from scipy.sparse import csr_matrix
from sklearn import metrics
from mpl_toolkits.mplot3d import Axes3D
from numpy import where
from collections import defaultdict
import collections
import matplotlib.patches as mpatches
import gzip

def connect_db():

    user = 'root'
    password = 'root'
    host = '127.0.0.1'
    database = 'book_reviews'

    print("\nConnecting to database......")

    conn = pymysql.connect(host=host, user=user, password=password,
                           db=database)

    return conn




def select_n_components(var_ratio, goal_var: float) -> int:
    # Set initial variance explained so far
    total_variance = 0.0

    # Set initial number of features
    n_components = 0

    # For the explained variance of each feature:
    for explained_variance in var_ratio:

        # Add the explained variance to the total
        total_variance += explained_variance

        # Add one to the number of components
        n_components += 1

        # If we reach our goal level of explained variance
        if total_variance >= goal_var:
            # End the loop
            break

    # Return the number of components
    return n_components



def getTSNE(vector):

    # TSNE for visualization
    tsne = TSNE(2, 100, learning_rate=500, n_iter=1000, verbose=True)
    tdoc = tsne.fit_transform(vector)

    return tdoc


def applyLSA(vector):

    # Perform dimensionality reduction using LSA

    t0 = time()
    # Vectorizer results are normalized, which makes KMeans behave as
    # spherical k-means for better results. Since LSA/SVD results are
    # not normalized, we have to redo the normalization.

    # Make sparse matrix
    vector_sparse = csr_matrix(vector)

    # Create and run an TSVD with one less than number of features
    tsvd = TruncatedSVD(n_components=vector_sparse.shape[1]-1)
    X_tsvd = tsvd.fit(vector)

    # List of explained variances
    tsvd_var_ratios = tsvd.explained_variance_ratio_

    # Get optimal n_components to be used in LSA
    n_components = select_n_components(tsvd_var_ratios, 0.95)
    print("\nn_components = {}\n".format(n_components))
    # Dimensionality reduction using LSA
    print("Performing dimensionality reduction using LSA")
    svd = TruncatedSVD(n_components)
    normalizer = Normalizer(copy=False)
    lsa = make_pipeline(svd, normalizer)
    vector = lsa.fit_transform(vector)

    print("\nDimensionality reduction done in %fs\n" % (time() - t0))

    explained_variance = svd.explained_variance_ratio_.sum()
    print("Explained variance of the SVD step: {}%".format(
        int(explained_variance * 100)))
    print()

    return vector


def applyTfidfVectorizer(df):

    print("\n\nExtracting features from the training dataset using a sparse vectorizer\n\n")
    t1 = time()
    # create the transform
    vectorizer = TfidfVectorizer()

    # tokenize and build vocab + encode document
    vector = vectorizer.fit_transform(df['text'])

    # summarize
    # print("\nSummarize...\n")
    # print("\n\nVocabulary...\n")
    # print(vectorizer.vocabulary_)
    # print("\n\nIDF...\n")
    # print(vectorizer.idf_)

    # summarize encoded vector
    # print("\nSummarize encoded vector...\n")
    # print("\nShape\n")
    # print(vector.shape)
    # print("\nTo array..\n")
    # print(vector.toarray())

    print("\n\nDone in {}s" .format(time() - t1))
    # print("\nn_samples: {}, n_features: {}\n" .format(vector.shape, vector.shape))
    print("n_samples %d, n_features: %d" % vector.shape)

    return vector


def getOptimalClusters(vector, tdoc):
    # Silhouette method to determine optimal clusters
    print("\n**********SILHOUETTE METHOD**********\n")

    range_n_clusters = range(2, 12)
    for n_cluster in range_n_clusters:

        kmeansModel = KMeans(n_clusters=n_cluster).fit(vector)
        label = kmeansModel.labels_

        # The silhouette_score gives the average value for all the samples.
        # This gives a perspective into the density and separation of the formed
        # clusters
        sil_coeff = silhouette_score(vector, label, metric='euclidean')
        print("\nFor n_clusters={}, the Silhouette Coefficient is {}".format(
            n_cluster, sil_coeff))

    # range_n_clusters = range(2,11)
    # for n_clusters in range_n_clusters:
    #     # Create a subplot with 1 row and 2 columns
    #     fig, (ax1, ax2) = plt.subplots(1, 2)
    #     fig.set_size_inches(18, 7)

    #     # The 1st subplot is the silhouette plot
    #     # The silhouette coefficient can range from -1, 1 but in this example all
    #     # lie within [-0.1, 1]
    #     ax1.set_xlim([-1, 1])
    #     # The (n_clusters+1)*10 is for inserting blank space between silhouette
    #     # plots of individual clusters, to demarcate them clearly.
    #     ax1.set_ylim([0, len(vector) + (n_clusters + 1) * 10])

    #     # Initialize the clusterer with n_clusters value and a random generator
    #     # seed of 10 for reproducibility.
    #     clusterer = KMeans(n_clusters=n_clusters, random_state=10)
    #     cluster_labels = clusterer.fit_predict(vector)

    #     # The silhouette_score gives the average value for all the samples.
    #     # This gives a perspective into the density and separation of the formed
    #     # clusters
    #     silhouette_avg = silhouette_score(vector, cluster_labels)
    #     print("For n_clusters =", n_clusters,
    #           "The average silhouette_score is :", silhouette_avg)

    #     # Compute the silhouette scores for each sample
    #     sample_silhouette_values = silhouette_samples(vector, cluster_labels)

    #     y_lower = 10
    #     for i in range(n_clusters):
    #         # Aggregate the silhouette scores for samples belonging to
    #         # cluster i, and sort them
    #         ith_cluster_silhouette_values = \
    #             sample_silhouette_values[cluster_labels == i]

    #         ith_cluster_silhouette_values.sort()

    #         size_cluster_i = ith_cluster_silhouette_values.shape[0]
    #         y_upper = y_lower + size_cluster_i

    #         cmap = cm.get_cmap("nipy_spectral")
    #         color = cmap(float(i) / n_clusters)
    #         ax1.fill_betweenx(np.arange(y_lower, y_upper),
    #                           0, ith_cluster_silhouette_values,
    #                           facecolor=color, edgecolor=color, alpha=0.7)

    #         # Label the silhouette plots with their cluster numbers at the middle
    #         ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

    #         # Compute the new y_lower for next plot
    #         y_lower = y_upper + 10  # 10 for the 0 samples

    #     ax1.set_title("The silhouette plot for the various clusters.")
    #     ax1.set_xlabel("The silhouette coefficient values")
    #     ax1.set_ylabel("Cluster label")

    #     # The vertical line for average silhouette score of all the values
    #     ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    #     ax1.set_yticks([])  # Clear the yaxis labels / ticks
    #     ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    #     # 2nd Plot showing the actual clusters formed
    #     cmap = cm.get_cmap("nipy_spectral")
    #     colors = cmap(cluster_labels.astype(float) / n_clusters)
    #     ax2.scatter(tdoc[:, 0], tdoc[:, 1], marker='.', s=30, lw=0, alpha=0.7,
    #                 c=colors, edgecolor='k')

    #     # Labeling the clusters
    #     centers = clusterer.cluster_centers_
    #     # Draw white circles at cluster centers
    #     ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
    #                 c="white", alpha=1, s=200, edgecolor='k')

    #     for i, c in enumerate(centers):
    #         ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
    #                     s=50, edgecolor='k')

    #     ax2.set_title("The visualization of the clustered data.")
    #     ax2.set_xlabel("Feature space for the 1st feature")
    #     ax2.set_ylabel("Feature space for the 2nd feature")

    #     plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
    #                   "with n_clusters = %d" % n_clusters),
    #                  fontsize=14, fontweight='bold')

    #     plt.show()

    # Elbow method
    print("\n**********ELBOW METHOD********\n")
    # sse = {}
    # for k in range(2, 11):
    #     kmeans = KMeans(n_clusters=k, max_iter=1000).fit(vector)
    #     label = kmeans.labels_
    #     #print(data["clusters"])
    #     sse[k] = kmeans.inertia_ # Inertia: Sum of distances of samples to their closest cluster center
    # plt.figure()
    # plt.plot(list(sse.keys()), list(sse.values()))
    # plt.xlabel("Number of cluster")
    # plt.ylabel("SSE")
    # plt.show()

    distorsions = []
    for k in range(2, 11):
        kmeans = KMeans(n_clusters=k).fit(vector)
        # Inertia: Sum of distances of samples to their closest cluster center
        distorsions.append(kmeans.inertia_)

    fig = plt.figure(figsize=(15, 5))
    plt.plot(range(2, 11), distorsions)
    plt.grid(True)
    plt.xlabel("Number of cluster")
    plt.ylabel("SSE")
    plt.title('Elbow curve')
    plt.show()


def applyKMeans(true_k, vector, tdoc):

    print("\nApplying k means...\n")
    km = KMeans(n_clusters=true_k, init='k-means++', max_iter=100, n_init=1,
                verbose=1)

    print("Clustering sparse data with %s" % km)
    t2 = time()
    km.fit(vector)
    print("\nDone in %0.3fs" % (time() - t2))
    print()

    labels = km.predict(vector)

    print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels, km.labels_))
    print("Completeness: %0.3f" %
          metrics.completeness_score(labels, km.labels_))
    print("V-measure: %0.3f" % metrics.v_measure_score(labels, km.labels_))
    print("Adjusted Rand-Index: %.3f"
          % metrics.adjusted_rand_score(labels, km.labels_))
    print("Silhouette Coefficient: %0.3f"
          % metrics.silhouette_score(vector, km.labels_, sample_size=1000))

    # Determine size per cluster
    # Will change range according to number of reviews
    print("\nSize per cluster..\n")
    P = km.predict(vector)
    for k in range(true_k):
        print(len([i for i in range(300) if P[i] == k]))

    # Plot the results
    # for i in set(km.labels_):
    #     index = km.labels_ == i
    #     plt.plot(vector[index, 0], vector[index, 1], 'o')
    # plt.show()
    for i in range(true_k):
        plt.scatter(tdoc[P == i, 0], tdoc[P == i, 1], marker='.')
    plt.show()

    #plot3D(vector, km.labels_)

    # centroids = km.cluster_centers_
    # labels = km.labels_
    # colors = ["g.","r.", "b."]

    # for i in range(true_k):
    #     print("coordinate:",vector[i], "label:", labels[i])
    #     plt.plot(vector[i][0], vector[i][1], colors[labels[i]], markersize = 10)

    # plt.scatter(centroids[:, 0],centroids[:, 1], marker = "x", s=150, linewidths = 5, zorder = 10)
    # print(centroids)
    # print(labels)
    # plt.show()


# Plots 2 features, with an output, shows the decision boundary
def plot3D(x, y):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    pos = where(y == 1)
    neg = where(y == 0)

    color = ['r', 'b', 'y', 'k', 'g', 'c', 'm']

    for i in range(30):
        ax.scatter(x[i, 0], x[i, 1], x[i, 2], marker='o', c=color[int(y[i])-1])
    #ax.scatter(x[:,1], x[:,2], y)
    ax.set_xlabel('X Label')
    ax.set_ylabel('Y Label')
    ax.set_zlabel('Z Label')

    axes = plt.axis()
    plt.show()

def parse(path):
  g = gzip.open(path, 'rb')
  for l in g:
    yield eval(l)

def getDF(path):
  i = 0
  df = {}
  for d in parse(path):
    df[i] = d
    i += 1
  return pd.DataFrame.from_dict(df, orient='index')


logging.basicConfig(
    format='[%(asctime)-15s] [%(name)s] %(levelname)s]: %(message)s',
    level=logging.DEBUG
)

# df = getDF('reviews_Books.json.json.gz')
# asin = ""
# df.loc[df['asin'] == asin]

# Connect to database
conn = connect_db()

# Change value according to the book id on books table
pd.set_option("display.max_colwidth", 10000)

# Querying reviews given book id
sql = "SELECT review_text from reviews"
df = pd.read_sql_query(sql, conn)

print("\nNumber of rows {}".format(len(df.index)))

# TFIDF
vector = applyTfidfVectorizer(df)

# Apply dimensionality reduction with LSA
vector = applyLSA(vector)

# TSNE
tdoc = getTSNE(vector)

# Get the optimal number of clusters
getOptimalClusters(vector, tdoc)


# Do the actual clustering

# applyKMeans(3, vector, tdoc)
