import re, string, unicodedata
import nltk
import contractions
import inflect
from bs4 import BeautifulSoup
from nltk import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from nltk.stem import LancasterStemmer, WordNetLemmatizer
import json

def strip_html(text):
    soup = BeautifulSoup(text, "html.parser")
    return soup.get_text()

def remove_between_square_brackets(text):
    return re.sub('<[^<]+?>', '', text)

def denoise_text(text):
    print("\nDenoising text.....")
    text = strip_html(text)
    text = remove_between_square_brackets(text)
    return text

def replace_contractions(text):
    """Replace contractions in string of text"""
    return contractions.fix(text)
def remove_non_ascii(words):
    """Remove non-ASCII characters from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = unicodedata.normalize('NFKD', word).encode('ascii', 'ignore').decode('utf-8', 'ignore')
        new_words.append(new_word)
    return new_words

def to_lowercase(words):
    """Convert all characters to lowercase from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = word.lower()
        new_words.append(new_word)
    return new_words

def remove_punctuation(words):
    """Remove punctuation from list of tokenized words"""
    new_words = []
    for word in words:
        new_word = re.sub(r'[^\w\s]', '', word)
        if new_word != '':
            new_words.append(new_word)
    return new_words

def remove_numbers(words):
    """Remove numbers from list of tokenize words"""

    new_words = []
    for word in words:
       if not word.isdigit():
        new_words.append(word)
    return new_words

def remove_words_lessthanequalthree(words):
    new_words = []
    for word in words:
       if len(word) > 3:
        new_words.append(word)
    return new_words

def remove_stopwords(words):
    """Remove stop words from list of tokenized words"""
    new_words = []
    for word in words:
        if word not in stopwords.words('english'):
            new_words.append(word)
    return new_words

def stem_words(words):
    """Stem words in list of tokenized words"""
    stemmer = LancasterStemmer()
    stems = []
    for word in words:
        stem = stemmer.stem(word)
        stems.append(stem)
    return stems

def lemmatize_verbs(words):
    """Lemmatize verbs in list of tokenized words"""
    lemmatizer = WordNetLemmatizer()
    lemmas = []
    for word in words:
        lemma = lemmatizer.lemmatize(word, pos='v')
        lemmas.append(lemma)
    return lemmas

def normalize(words):
    words = remove_non_ascii(words)
    words = to_lowercase(words)
    words = remove_punctuation(words)
    #words = replace_numbers(words)
    words = remove_stopwords(words)
    return words

def stem_and_lemmatize(words):
    stems = stem_words(words)
    lemmas = lemmatize_verbs(words)
    return stems, lemmas


sample = "\"Read Harry Potter!\" href='https://readrantrockandroll.com/2018/01/28/shabby-sunday-owl-moon-by-jane-yolen-and-john-schoenherr-1987/' they said. \"It'll be fun!\" they said. \"Our childhood was built on Harry Potter!\" they said.\n\nWELL YOUR CHILDHOODS WERE ALL HORRIBLE. OMG WHAT IS THIS BOOK? WHAT IS THIS MESS I'M IN?? Excuse me, but that large salty lake where my life used to be is my <I>tears</i>.  <B>Yes, yes, this means I loved it.</b> But SERIOUSLY. Nooooot okay. At the end there, especially, I couldn't put the audio down so I was wandering about my house in a blind daze listening instead.\n\nAnd because every human and their fluffy poodle has probably sensibly reviewed this book, BAH. I'm just going to make a list of things I loved and things I didn't. \n\n<b>My Intense And Passionate LOVES:</b>\n• Harry + Ginny = yes. I was dubious at first. Like I think romance is NOT JKR's strong point...plus when she gets to a romantic scene she ends the chapter. HHAHAH. ADORABLE. (Not that I minded, I just...I think she's not good at romance.) But omg, there was that moment after the Quidditch match??? YUP. You know what I'm talking about. Excuse me, my heart just ran around in circles screaming and shipping.\n• Hermione: I adore her, but...I gotta admit, she was a little bit too stuffy. BUT I STILL LOVE HER. I love her love for learning.\n• I also adore how sensible Ginny is. \n• Fred and George's joke shop gives life. Also death, probs, but liiiiife.\n• I am utterly in LOVE with the whole concept of Hogwarts. I mean, I think it's stupid that they have to write with a quill and ink (can't they just enchant the quill to write for them?) and they seem to get an excessive amount of homework, BUT. It's all so vivid and detailed and it literally feels like Hogwarts is A REAL PLACE. I totally get why people wail about their letters going missing. \n• Speaking of detail...me likey. I always felt <i>inside</i> the story.\n• Plus the audio book (narrated by Jim Dale) is GREAT. I totally recommend it. I think Hermione's voice had a tendency to sound like an insufferable goat, but I got used to it. Harry's voice was perfection. \n• I also particularly liked Harry in this book. I JUST LOVE HARRY. <3 He's snarky, but he's lost some of the angriness from bk 5. He's got snarky quips at the ready, but he's also intensely...good. Like he's just a really GOOD chap, who makes bad decisions but really means well. ZOMG. *flails about* He was definitely my favourite character in this book. \n• ALSO DUMBLEDORE WAS GREAT. I love Dumbledore and how he's always polite and kind and smiley. <spoiler> CAN WE TALK A MINUT EHOW I'M NOT OKAY THAT HE'S DEAD?!??!?!? LIKE WHAT EVEN THE FLYING HECK OF BROOMS NEST TAILS. THAT DOESN'T MAKE SENSE. BUT IT SHOULDN'T. BECAUSE NOTHING MAKES SENSE. Dumbledore. <I>no</i>. Come baaaaaaaack. *breaks down into a mess* I am also intensely furious that his weakening himself to get the horcrux was useless. MAD. Okay? I AM MAD. Character deaths where they failed are the worst there are. </spoiler>\n\n<B>My Intense and Passionate LOATHES:</b>\n<i>(Hhahahha...you ready for this?)</i>\n\n• Okay so, question: WHY do they sometimes go places and buy food...and sometimes they just magic it? Like Mrs. Weasely magics onion soup out of nothing for Harry. But in the next chapter, Harry and Ron are peeling sprouts. LIKE WUT. This makes no sense to me. \n• Why didn't Dumbledore pour the evil potion on the ground instead of drinking it? Just wannna know...\n• How the FLYING FRIKKIN BROOMSTICKS can anyone possibly convince me Snape is still on the good side? Because I don't even care. He can go abra kadabra himself to Timbuktu and DIE THERE THANKS. There is no repentance after this. I don't even care what the \"reason\" is in book 7, but Harry: you are an idiot to name your kid after Snape. \n• I HOPE SNAPE DIES.\n• DID I MENTION MY HATE FOR SNAPE YET???????\n• Oh and another thing...<I>I am so angry at Snape arghhhqqwqq.</i>\n• Okay. Good now. I'm calm. Thank you. \n• HAHHAHHAHAHTE SNAPE.\n• Yes, I truly am calm this time. I'm good.\n• <I>So</i>. I also am fairly annoyed about Voldermort's (Tom Riddle) backstory. Because he read like he was straight out of a psychopath text book...which I found monstrously unimaginative. Attractive, calm, subtle bully, kleptomaniac, limited emotional reach, charmer, smooth...blah blah. Yes. I can just read a book about psychopaths and that's Voldey's description. I wish there'd been a little more ingenuity in there.\n• I really also dislike Ron. I KNOW. Travesty. But he's a bully! He was bullying first year's <I>the whole time</i>...turfing tem off chairs, scaring them, bossing them around, taking stuff off them in the name of \"prefect\". Like how is that good?? How is that okay??? I will not forgive Ron for all this. And also the fact that he's a downright BRUTE to Hermione. How can I ship them when he's so rude and demeaning to her?? I DON'T. Hermione deserves better. And I don't buy the \"She completes him by sorting him out\" because I think all humans should take responsibility for their actions. Hermione shouldn't HAVE to force Ron into a better person and be his conscious. AGH. \n• Not shipping that. ^^\n• Okay, I'm done. But just *whispers* I hate hate hate Snape. \n\n<B>So basically YES, I had a fantastic time with this book.</b> I loved the audio. I loved the story. I <i>loved</i> Harry and the world just feels so big and rich and visual. I get the intense fandom for this I DO GET IT. After being so angry and 1000% done in with book 3, I can successfully say I can't WAIT to read the last book now. And then watch all the movies. And then I'll finally be caught up, omg. Only took me like 10 years. "
# print("Original text...\n\n")
# print(sample)
#This book is a Shabby Sunday feature @ <a target="_blank" href="https://readrantrockandroll.com/2018/01/28/shabby-sunday-owl-moon-by-jane-yolen-and-john-schoenherr-1987/" rel="nofollow">https://readrantrockandroll.com/2018/...</a><br /><br /><i>Owl Moon</i> is a striking story that takes you on a journey through the winter woods in search of owls. The little child has been waiting to go owling with Pa for a very long time. The story rather reads like poetry.<br /><br /><i>"Our feet crunched over the crisp snow and little gray footprints followed us. Pa made a long shadow, but mine was short and round. I had to run after him every now and then to keep up, and my short, round, shadow bumped after me."</i><br /><br />John Schoenherr's illustrated imagery paints the perfect winter impression and this is a ideal book for bedtime that highlights the companionship between parent and child. Our copy is an old Scholastic paperback edition from 1988. It’s in fairly good shape with clean pages and one to keep.<br /><br />5*****
#Denoise text
#sample = BeautifulSoup(sample, "lxml").text
cleanText = BeautifulSoup(sample, "lxml").text
#sample = denoise_text(sample)
# print("\nDenoised text...\n")
print(cleanText)



# #Replace contractions 
# sample = replace_contractions(sample)
# # print("\n\nWithout contractions text...\n")
# # print(sample)s


# words = nltk.word_tokenize(sample)
# # print("\n\nTOKENIZED \n")
# # print(words)

# #Remove numbers
# words = remove_numbers(words)

# #Remove words with <= 3 letters
# words = remove_words_lessthanequalthree(words)

# #Normalize tokens
# words = normalize(words)
# print(words)

# print("\nSTEMMING AND LEMMATIZATION\n")
# stems, lemmas = stem_and_lemmatize(words)
# print('Stemmed:\n', stems)
# print('\nLemmatized:\n', lemmas)


# with open('neww.txt', 'w', encoding='utf-8') as outfile:
#     json.dump(words, outfile, sort_keys = True, indent = 4, ensure_ascii = False)

# print("Done...")